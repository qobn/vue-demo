import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
// 这个东西是单页面的 刷新后 这个数据就没了，所以在App.vue中监听刷新并存入sessionStorage中
Vue.use(Vuex)
export default new Vuex.Store(
  {
   modules:{
     user
   }
  }
)


