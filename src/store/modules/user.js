const user = {
//全局state对象，用户保存所有组件中的公共数据
   state : sessionStorage.getItem('state') ? JSON.parse(sessionStorage.getItem('state')) : {
    user: {
      name: ''
    }
  },
//监听state对象的值的最新动态（计算属性）
   getters :{
    getUser(state) {
      return state.user
    }
  },
//唯一一个可以修改state值的方法(同步执行，阻塞方法)
   mutations : {
    updateUser(state, user) {
      state.user = user
    }
  },
//异步执行mutation方法
   actions : {
    asyncUpdateUser(context, user) {
      context.commit('updateUser', user)
    }
  }

}
export default user
