// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router'
import axios from 'axios'
import Vuex from 'vuex'
import store from './store'

Vue.use(ElementUI);
Vue.use(Vuex);
Vue.config.productionTip = false;
Vue.prototype.axios = axios;

//路由插件 放在main.js里每次都执行
router.beforeEach((to,from,next)=>{
  let isLogin = sessionStorage.getItem('isLogin')
  console.log('isLogin:='+isLogin+ ' 本地存储登录标记是否为空 '+(isLogin==null))
  if (isLogin!=null){
    console.log('1')
    if (to.path==='/logout'){
      sessionStorage.clear()
      console.log('2')
      next({
        path:'/'
      })
    } else{
      console.log('3')
      next()
    }
  }else{
    if (to.path==='/'){
      next()
    } else if (to.path==='/Home') {
      next()
    }
  }
  console.log('to.path:='+to.path)
  console.log('from.path:='+from.path)
  console.log('next.path:='+next.path)
})
/* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   router,
//   components: { App },
//   template: '<App/>'
// })
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
});
