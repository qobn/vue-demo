import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import NotFound from '@/components/NotFound'
import Login from '@/components/Login'
import CenterAll from '@/components/ShopManager/CenterAll'
import CityAll from '@/components/ShopManager/CityAll'
import TestChild from '@/components/ShopManager/TestChild'
import Joke from '@/components/ShopManager/Joke'

Vue.use(Router);
export default new Router({
  mode:'history', //设置mode模式为history则页面路径去掉#  带#属于hash模式 不带#属于history模式
  routes: [
    {
      path: '/Home',
      name: 'Home',
      component: Home,
      children: [
        {
          path: '/CenterAll/:id', //这个不用改
          name: 'CenterAll',
          component: CenterAll,
          props:true
          // 可以用props方式传参
        },
        {
          path: '/Joke',
          name: 'Joke',
          component: Joke,
        },
        {
          path: '/CityAll',
          name: 'CityAll',
          component: CityAll,
          children:[
            {
              path: '/TestChild',
              name: 'TestChild',
              component: TestChild
            },
          ]
        },
      ]
    },
    {
      path: '/',
      name: 'Login',
      component: Login,
    },
    {
      path:'/goHome',
      redirect:'/Home'
    },
    {
      path:'/logout',
    },
    {
      path:'*',
      component:NotFound
    }
  ]
})
