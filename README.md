# vue-element

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


##### 初始化项目
vue init webpack 项目名称
##### 安装网络请求框架
npm install --save axios vue-axios
##### 安装vuex
npm install vuex@next --save

-----
##### node_modules 不匹配
npm install

